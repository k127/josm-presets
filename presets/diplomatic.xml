<?xml version="1.0" encoding="UTF-8"?>
<presets xmlns="http://josm.openstreetmap.de/tagging-preset-1.0"
  author="k127"
  version="1.1_2019-10-22"
  link="https://gitlab.com/k127/josm-presets/blob/master/diplomatic.xml"
  shortdescription="Diplomatic and consular missions"
  description="Implements https://wiki.openstreetmap.org/wiki/DE:Tag:office=diplomatic schema">

    <group name="Facilities">

        <group name="Public Building">

            <!-- this preset is built upon the approved schema of Tag:office=diplomatic, and, secondarily, also respects tags being in the process of deprecation: Tag:amenity=embassy -->

            <item icon="presets/service/embassy.svg" type="node,closedway,multipolygon" description="Inspired by https://maproulette.org/challenge/2791"
                name="Mission or parastatal entity">

                <label icon="presets/service/embassy.svg"
                    text="Mission or parastatal entity" />

                <space />

                <key key="amenity" value="embassy" /><!-- legacy; in process of deprecation -->

                <key key="office" value="diplomatic" />

                <combo key="country" values_from="java.util.Locale#getISOCountries"
                    text="Country code for sending country or organization" />

                <combo key="target" values_from="java.util.Locale#getISOCountries"
                    text="Country code for accrediting country or organization" />

                <text key="name"
                    text="Official name of the mission (w/o location, language of accrediting country)" />

                <combo key="diplomatic"
                    text="Type of diplomatic institution">

                    <list_entry value="embassy"
                        display_value="Embassy or diplomatic mission" />

                    <list_entry value="consulate"
                        display_value="Consulate" />

                    <list_entry value="liaison"
                        display_value="Liaison office" />
                </combo>

                <space />

                <!-- TODO extract this into a sub-item -->
                <label
                    text="If diplomatic=embassy:" />

                <combo key="embassy"
                    text="Type of embassy or diplomatic mission">

                    <list_entry value="yes"
                        display_value="Embassy, headed by an ambassador" />

                    <list_entry value="branch_embassy"
                        display_value="Branch of an embassy, headed by a diplomat below ambassadorial rank" />

                    <list_entry value="delegation"
                        display_value="Delegation, headed by an ambassador" />

                    <list_entry value="high_commission"
                        display_value="High commission, headed by a high commissioner" />

                    <list_entry value="interests_section"
                        display_value="Diplomatic mission sent under a protecting power, signed as an 'interests section' and headed by a 'principal officer'" />

                    <list_entry value="mission"
                        display_value="Diplomatic mission to or from a multilateral organization such as the United Nations" />

                    <list_entry value="nunciature"
                        display_value="Diplomatic mission from the Vatican, headed by a nuncio" />

                    <list_entry value="residence"
                        display_value="Official residence of an ambassador or other diplomatic chief of mission" />
                </combo>

                <space />

                <!-- TODO extract this into a sub-item -->
                <label
                    text="If diplomatic=consulate:" />

                <combo key="consulate"
                    text="Type of consulate">

                    <list_entry value="yes"
                        display_value="Consulate headed by a consul" />

                    <list_entry value="consular_agency"
                        display_value="Only consular services to citizens of the sending country (no visa services)" />

                    <list_entry value="consular_office"
                        display_value="Limited consular services" />

                    <list_entry value="consulate_general"
                        display_value="Consulate general, headed by a 'consul general'" />

                    <list_entry value="honorary_consul"
                        display_value="Office of a host-country national who offers limited services on behalf of a foreign country" />
                </combo>

                <space />

                <!-- TODO extract this into a sub-item -->
                <label
                    text="If diplomatic=liaison:" />

                <combo key="liaison"
                    text="Type of diplomatic liaison office">

                    <list_entry value="liaison_office"
                        display_value="Unaccredited diplomatic office of a country lacking full diplomatic recognition by the host country" />

                    <list_entry value="representative_office"
                        display_value="Unaccredited diplomatic office of a government-in-exile" />

                    <list_entry value="subnational"
                        display_value="Non-diplomatic office abroad of a subnational (provincial or state) government" />
                </combo>

                <optional>
                    <text key="short_name"
                        text="Colloquial name (w/o location, language of accrediting country)" />
                    <!-- the precursory property is not mentioned in the Tag:office=diplomatic schema -->

                    <label
                        text="Services offered:" />

                    <check key="diplomatic:services:non-immigrant_visas"
                        text="Non-immigrant visas (e.g. tourist or visitor visas) available" />

                    <check key="diplomatic:services:immigrant_visas"
                        text="Immigrant or permanent residence visas available" />

                    <check key="diplomatic:services:citizen_services"
                        text="Passport and other document services for citizens of the sending country" />
                </optional>

                <space />

                <link wiki="Tag:office=diplomatic" />

                <!--link wiki="Key:diplomatic" />

                <link wiki="Key:embassy" />

                <link wiki="Key:consulate" />

                <link wiki="Key:liaison" /-->

                <preset_link preset_name="Embassy" /><!-- legacy; in process of deprecation -->

            </item>
        </group>
    </group>
</presets>
